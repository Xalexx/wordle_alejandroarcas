import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.io.PrintWriter
import java.nio.file.Path
import kotlin.io.path.forEachLine

/**
 * Function that verifies if a String has five letters
 *
 * @author Alejandro Arcas
 * @since 1.0.0
 * @param word input type
 * @return [Boolean]
 */
fun checkInputWord(word: String) : Boolean {
    return word.length != 5
}

/**
 * Function that verifies if a String has a number
 *
 * @author Alejandro Arcas
 * @since 1.0.0
 * @param word input type
 * @return [Boolean]
 */
fun checkInputNumber(word: String) : Boolean {
    val userWordUppercase = word.uppercase()
    for (i in userWordUppercase.indices){
        if (userWordUppercase[i] !in 'A'..'Z') return true
    }
    return false
}

/**
 * Function that generates a random word from a list, and converts it to Uppercase
 *
 * @author Alejandro Arcas
 * @since 1.0.0
 * @param list input type
 * @return [String] with a random word to Uppercase
 */
fun checkInput(){
    while (checkInputWord(userWord) || checkInputNumber(userWord)){
        if (checkInputNumber(userWord) && checkInputWord(userWord)) println(redColor + "ERROR: La palabra introducida no puede contener numeros y debe tener 5 letras$resetColor")
        else if (checkInputWord(userWord)) println(redColor + "ERROR: La palabra introducida debe tener 5 letras$resetColor ")
        else if (checkInputNumber(userWord)) println(redColor + "ERROR: La palabra introducida no puede contener numeros$resetColor")
        println(purpleColor + "Introduce de nuevo la palabra:$resetColor")
        userWord = sc.next().uppercase()
    }
}

/**
 * Function that paints the letters of the word entered by the user
 *
 * @author Alejandro Arcas
 * @since 1.0.1
 * @param userWord word entered by the user
 * @param secretWord word generated randomly
 */
fun paintLetters(userWord: String, secretWord: String){
    var yellowLetters = 0
    for (i in userWord.indices){
        if (secretWord[i] == userWord[i]){
            print("$greenColor${userWord[i]}$resetColor")
        } else if (secretWord.contains(userWord[i])){
            if (repeatedLetters(userWord, userWord[i]) <= repeatedLetters(secretWord, userWord[i])) {
                print("$yellowColor${userWord[i]}$resetColor")
            }
            else if (yellowLetters < repeatedLetters(secretWord, userWord[i]) - guessedLetters(secretWord, userWord, userWord[i])) {
                print("$yellowColor${userWord[i]}$resetColor")
                yellowLetters++
            } else print("$greyColor${userWord[i]}$resetColor")
        } else print("$greyColor${userWord[i]}$resetColor")
    }
}

/**
 * Function that counts how many letters are repeated in the word entered by the user
 *
 * @author Alejandro Arcas
 * @since 1.0.0
 * @param userWord word entered by the user
 * @param letter letter to compare if it is the same
 * @return [Int] counter of repeated letters in the word
 */
fun repeatedLetters(userWord: String, letter: Char) : Int {
    var counter = 0
    for (i in userWord.indices){
        if (userWord[i] == letter) counter++
    }
    return counter
}

/**
 * Function that counts how many letters are correct in the user's word
 *
 * @author Alejandro Arcas
 * @since 1.0.0
 * @param userWord word entered by the user
 * @param secretWord word generated randomly
 * @param letter letter to compare if it is correct
 * @return [Int] counter of guessed letters in the word
 */
fun guessedLetters(userWord: String, secretWord: String, letter: Char) : Int {
    var counter = 0
    for (i in userWord.indices){
        if (secretWord[i] == userWord[i] && userWord[i] == letter && secretWord[i] == letter) counter++
    }
    return counter
}

/**
 * Function that reads a file and adds the content to a mutableList to later generate a random word from that list
 *
 * @author Alejandro Arcas
 * @since 1.0.0
 * @param fileName select created file
 * @param randomWords mutableList to add words from file
 * @return [String] word generated randomly from the mutableList
 */
fun generateWord(fileName: Path, randomWords: MutableList<String>): String {
    fileName.forEachLine { randomWords.add(it) }
    return randomWords.random().uppercase()
}

/**
 * Function that writes to the ranking file with a specific format
 *
 * @author Alejandro Arcas
 * @since 1.0.0
 * @param fileName ranking file
 */
fun writeRankingFile(fileName: File) {
    val listOfContent = mutableListOf<MutableList<String>>()
    val contentOfFile = fileName.readText()
    BufferedReader(FileReader(fileName)).use { br ->
        var line: String?
        while (br.readLine().also { line = it } != null) {
            listOfContent.add(line?.split(' ') as MutableList<String>)
        }
    }
    for (i in listOfContent.indices){
        if (listOfContent[i][0] == userName){
            val oldScore = score
            score += listOfContent[i][1].toInt()
            val newScore = score
            val writer = PrintWriter(fileName)
            writer.close()
            fileName.appendText(contentOfFile.replace("$userName $oldScore","$userName $newScore"))
        }
        else {
            fileName.appendText("$userName $score")
            fileName.appendText("\n")
        }
        break
    }
}

/**
 * Function that reads the ranking file and displays the content with a specific format
 *
 * @author Alejandro Arcas
 * @since 1.0.0
 * @param fileName ranking file
 */
fun readRankingFile(fileName: File){
    BufferedReader(FileReader(fileName)).use { br ->
        var line: String?
        while (br.readLine().also { line = it } != null) {
            rankingContent.add(line?.split(' ')!!.toMutableList())
        }
    }
    rankingContent.sortByDescending { it[1].toInt() }
    for (i in 0..rankingContent.lastIndex) {
        println("$redColor${rankingContent[i][0]} $resetColor - $blueColor ${rankingContent[i][1]} pt")
    }
}