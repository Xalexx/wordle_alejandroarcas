import java.io.File
import java.util.Scanner
import kotlin.io.path.Path
import kotlin.system.exitProcess

// Scanner definition
val sc = Scanner(System.`in`)

// Create variables with different colors
const val greyColor = "\u001b[37m"
const val yellowColor = "\u001B[33m"
const val greenColor = "\u001b[32m"
const val resetColor = "\u001b[0m"
const val purpleColor = "\u001B[35m"
const val redColor = "\u001B[31m"
const val blueColor = "\u001B[34m"

var randomWords = mutableListOf<String>()
var rankingContent = mutableListOf<MutableList<String>>()

var secretWord = ""
var userWord = ""
var userName = ""
var score = 0

var rankingFile = File("./files/ranking.txt")
val spanishWordsFile = Path("./files/spanish.txt")
val englishWordsFile = Path("./files/english.txt")

fun main() {
    do {
        score = 0
        randomWords = mutableListOf()

        // Welcome message
        println(redColor + "Bienvenido a " + blueColor + "WORDLE!" + resetColor)
        println(
            "1. Jugar con palabras en Castellano \n" +
                    "2. Jugar con palabras en Ingles \n" +
                    "3. Historico de partidas \n" +
                    "0. Salir del juego"
        )
        when (sc.nextInt()) {
            1 -> {
                secretWord = generateWord(spanishWordsFile, randomWords)
            }
            2 -> {
                secretWord = generateWord(englishWordsFile, randomWords)
            }
            3 -> {
                println("${yellowColor}Ranking Wordle$resetColor")
                readRankingFile(rankingFile)
                println(yellowColor + "___________________________$resetColor")
                exitProcess(0)
            }
            0 -> exitProcess(0)
        }

        println(blueColor + "Indica tu nombre de usuario:")
        userName = sc.next()
        println("$purpleColor$userName, adivina la palabra secreta de 5 letras en solo 6 intentos, cada vez que escribas una palabra recibiras pistas.")
        println("Escribe una palabra de 5 letras para empezar:$resetColor")

        var attempts = 0

        do {
            userWord = sc.next().uppercase()

            // Loop to check all error possibilities of the user input word
            checkInput()

            // Loop to paint the letters according to the rules of the game
            println(purpleColor + "Resultado de la palabra:$resetColor")
            paintLetters(userWord, secretWord)
            attempts++
            println("$purpleColor        Intentos: $attempts")

        } while (userWord != secretWord && attempts != 6)

        // Conditional to indicate the end of the game
        if (attempts == 6 && userWord != secretWord) {
            println(blueColor + "Lasitma, has gastado tu maximo de intentos!\nLa palabra secreta era: $greenColor$secretWord$resetColor\n${blueColor}Final.$resetColor")
            score = 1
            writeRankingFile(rankingFile)
        } else {
            println(blueColor + "Bravo! has acertado la palabra $greenColor$secretWord$resetColor$blueColor con un total de: $resetColor$purpleColor$attempts$resetColor$blueColor intentos \nFinal.$resetColor")
            score = 5
            writeRankingFile(rankingFile)
        }

        // Repeat game
        println("¿Quieres volver a jugar?")
        println("Escribe: Si o No")
        val play = sc.next().lowercase()

    } while (play == "si")
}






