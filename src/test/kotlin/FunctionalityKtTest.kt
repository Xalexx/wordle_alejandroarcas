import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class FunctionalityKtTest {

    @Test
    fun checkInputWord() {
        assertTrue(checkInputWord("adioss"))
        assertTrue(checkInputWord("adio"))
        assertFalse(checkInputWord("adios"))
        assertFalse(checkInputWord("ADIOS"))
    }

    @Test
    fun checkInputNumber() {
        assertTrue(checkInputNumber("adio5"))
        assertTrue(checkInputNumber("55555"))
        assertFalse(checkInputNumber("ADIOS"))
        assertFalse(checkInputNumber("adios"))
    }

    @Test
    fun repeatedLetters() {
        assertEquals(repeatedLetters("adios", 'l'), 0)
        assertEquals(repeatedLetters("adios", 'a'), 1)
        assertEquals(repeatedLetters("aaios", 'a'), 2)
        assertEquals(repeatedLetters("iiiii", 'i'), 5)
    }

    @Test
    fun guessedLetters() {
        assertEquals(guessedLetters("arcen", "movil", 'a'), 0)
        assertEquals(guessedLetters("adios", "avion", 'a'), 1)
        assertEquals(guessedLetters("aadio", "aavio", 'a'), 2)
        assertEquals(guessedLetters("iiiii", "iiiii", 'i'), 5)
    }
}