# WORDLE_Alejandro

## Descripción

A continuación encontrarás el juego llamado Wordle, es un juego de adivinar palabras en formato de crucigrama y muy similar a juegos como Mastermind. Tienes que adivinar la palabra en un máximo de seis intentos, en los que no te dan más pistas que decirte que letras has acertado en su posición y que otras letras contiene la palabra, pero están en su posición incorrecta, además si la letra no está contenida en la palabra secreta se pintará de un color grisáceo.

## Instalación

- Copiar la dirección HTTPS del repositorio

![imgClone.png](imgClone.png)

- Abrir el Entorno de Desarrollo Integrado (IDE) que es Intellij IDEA
- Crear un proyecto nuevo mediante un Control de Versiones
- Pegar la dirección del proyecto y clonar

![CloneVersionImg.png](CloneVersionImg.png)

## Ejecución

- Entrar en la siguiente ruta absoluta: /wordle_alejandro/src/main/kotlin
- Ejecutar con clic derecho el archivo "Main.kt"
- Run 'Main.kt'

![EjecucionImg.png](EjecucionImg.png)

## Instrucciones de juego

Una vez de haya ejecutado el programa, las instrucciones de juego son las siguientes:
- Tendrás acceso a un menu con diferentes opciones:
  - Habra la posibilidad de escoger la partida con palabras en ingles o en castellano
  - Tambien tendrás la posibilidad de acceder al ranking y a la opción de cerrar el programa
- Se generará una palabra aleatoria dentro de un archivo de mas de 100 palabras
- Deberás ingresar una palabra, dentro de estas normas:
    - La palabra debe contener 5 letras
    - No puede contener numeros
- Por cada palabra ingresada se sumará un intento (con un máximo de seis)
- Se darán pistas por cada palabra que se indique, tal y como se explica en la descripción
- Al final del juego se lanzará un mensaje de despedida en funcion de si has ganado o has perdido
- Cada vez que termines una partida se guardará el nombre de usuario y la puntuación obtenida en el ranking.

## Autores
Alejandro Arcas León

## Licéncia
MIT License

Copyright (c) 2022 Alejandro Arcas Leon